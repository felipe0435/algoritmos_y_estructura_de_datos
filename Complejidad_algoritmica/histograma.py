import random
import time


def calcular_rango(datos):
    """Calcula el número mayor y menor entre los datos y el rango"""
    mayor = 0
    menor = datos[0]
    for dato in datos:
        if dato > mayor:
            mayor = dato
        elif dato < menor:
            menor = dato
    rango = mayor - menor

    return rango, menor, mayor


def crear_intervalo(rango, mayor):
    """Crea la cantidad de intérvalos y amplitud del histograma"""
    intervalos = (mayor) ** 0.5
    entero = int(intervalos)
    decimales = intervalos - entero
    if decimales >= 0.5:
        intervalos = entero + 1
    else:
        intervalos = entero

    if rango % intervalos != 0:
        aux = rango / intervalos
        entero = rango // intervalos
        decimales = aux - entero
        if decimales >= 0.5:
            #h = amplitud del histograma
            h = entero + 1
        else:
            h = entero
    else:
        h = rango / intervalos

    return int(h), int(intervalos)


def crear_histograma(datos):
    """Crea el histograma"""
    rango, menor, mayor = calcular_rango(datos)
    h, intervalos = crear_intervalo(rango, mayor)
    grafica = [[] for _ in range(intervalos)]
    histograma = [[menor + i * h, menor + h * i + h] for i in range(intervalos)]
    histograma[len(histograma) - 1][1] = mayor

    for dato in datos:
        for i in range(intervalos):
            if dato >= histograma[i][0] and dato < histograma[i][1]:
                grafica[i].append(dato)
                break

    for i in range(len(grafica)):
        histograma[i].append(histograma[i][1])
        histograma[i][1] = f"({len(grafica[i])})"

    print(histograma)


def main():
    """Función main"""
    tiempos = []
    for i in range(2,8):
        tot = 0
        for _ in range(10):
            cant_datos = 10**i
            datos = [random.randint(0, 800) for _ in range(cant_datos)]
            inicio = time.time()
            crear_histograma(datos)
            fin = time.time()
            tiempo = fin - inicio
            print(tiempo)
            tot += tiempo

        prom = tot / 10
        tiempos.append(prom)
    print(tiempos)


if __name__ == '__main__':
    main()
